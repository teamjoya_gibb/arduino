/*
 Name:		M121_Projektarbeit.ino
 Created:	6/10/2018 11:07:23 AM
 Author:	Yannik Gartmann
*/

// Library includen
#include <ArduinoJson.h>
#include <Console.h>
#include <Ethernet.h>
#include <SPI.h>

//Pin Variables
int tempAnalogPin = 0;
int whaterLevelAnalogPin = 2;
int moistureAnalogPin = 1;

// Config Variables f�r Netzwerkkarte
byte mac[] = { 0xB4, 0x22, 0x8A, 0xF8, 0x45, 0x11 };
IPAddress ip(192,168,178,40);
IPAddress ns(192,168,178,1);
IPAddress gw(192,168,178,1);
byte mask[] = {255,255,255,0};

// Zielserver
char host[] = "194.209.222.197";

void setup() {
	// LED Pin (Onboard LED) setzen
	pinMode(LED_BUILTIN, OUTPUT);

	// Bridge und Console konfigurieren
	Bridge.begin();
	Console.begin(); 

	while(!Console); // Auf Console warten --> Bitte auskommentieren bei Produktion!
  
	Console.println("Setup started!");

	// Netzwerkkarte konfigurieren
	Ethernet.begin(mac ,ip, ns, gw, mask);
	delay(2000); // 2 Sekunden warten, damit sich das Arduino im Netz anmelden kann

	// Local IP ausgeben
	Console.print("Local IP: ");
	Console.println(Ethernet.localIP());

	Console.println("Setup completed!");
	Console.print("Using URL: ");
	Console.print(host);
	Console.println();
}

void loop() {
	  Console.println("Getting Sensordata ...");

	// Sensorenwerte lesen
	int tempSensor = analogRead(tempAnalogPin);
	int whaterLevelSensor = analogRead(whaterLevelAnalogPin);
	int moistureSensor = analogRead(moistureAnalogPin);  
	// ACHTUNG: KEIN Lichtsensor eingebuat!
	// --> Wird bei Bedarf realisiert :)

	// Daten senden
	sendData(0, tempSensor, moistureSensor, whaterLevelSensor);

	delay(30000); // 30 Sekunden warten, damit der Server nicht mit daten "Bombadiert" wird.
}

void sendData(int LightSensor, int TempSensor, int MoistureSensor, int WaterLevelSensor) {
	Console.println("Starting request ...");
	
	// JSON Buffer setzten
	StaticJsonBuffer<200> jsonBuffer;

	// JSON zusammenbauen
	JsonObject& data = jsonBuffer.createObject();
	data["LightSensor"] = LightSensor;
	data["TempSensor"] = TempSensor;
	data["MoistureSensor"] = MoistureSensor;
	data["WaterLevelSensor"] = WaterLevelSensor;

	// Daten aus JSON ausgeben
	Console.println("Data as JSON:");
	data.prettyPrintTo(Console);
	Console.println();

	// EthernetClient vorbereiten
	EthernetClient client;
	client.setTimeout(10000);

	if (client.connect(host, 80)) // Mit Server auf Port 80 verbinden (HTTP)
	{
		Console.println("Connected to server");

		// HTTP Request zusammenbauen   
		client.println("PUT /api/1 HTTP/1.1");
		client.println("Host: modul121.scapp.io");
		client.println("Content-Type: application/json");
		client.println("ARDUINO_TOKEN: gibbiX12345");
		client.println("Connection: keep-alive");
		client.println("User-Agent: arduino");
		client.print("Content-Length: ");
		client.println(data.measureLength());
		client.println();
		data.printTo(client);

		// LED bei erfolgreichen Request ausschalten, da alles ja wieder funktioniert
		digitalWrite(LED_BUILTIN, LOW);
		Console.println("Success request");
	}
	else
	{
		// LED bei Fehler einschalten und somit mitteilen, dass etwas nicht funktioniert
		digitalWrite(LED_BUILTIN, HIGH);
		Console.println("Request failed");
	}
}
